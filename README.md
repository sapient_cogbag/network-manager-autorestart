# network-manager-autorestart
Simple program which periodically checks network connectivity and in case of failure disconnects then reconnects NetworkManager, to fix a weird problem with networking on my desktop/server.

***If you are on GitHub, this has been moved to:https://gitlab.com/TheEdenCrazy/network-manager-autorestart***
